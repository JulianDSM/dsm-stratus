<?php
//
// The index (fallback) page template
//

get_header();
?>
<div class="page">
  <?php
  if (have_posts()) :
    while (have_posts()) :
      the_post();

      get_template_part('templates/modules/content/content');
    endwhile;
  endif;
  ?>
</div>
<?php get_footer() ?>