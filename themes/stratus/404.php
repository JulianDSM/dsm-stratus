<?php
//
// The 404 page template
//

get_header();
?>
<div class="page">
  <?php get_template_part('templates/modules/content/not-found') ?>
</div>
<?php get_footer() ?>