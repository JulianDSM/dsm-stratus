<?php
//
// The Case Study post page template
//

get_header();
?>
<!--single-case-studies.php-->
<div class="page">
  <?php
  if (have_posts()) :
    the_post();

    get_template_part('templates/modules/banners/page-banner');
    get_template_part('templates/modules/content/case-study-content');
    get_template_part('templates/loops/sections');
  else:
    get_template_part('templates/modules/content/not-found');
  endif;
  ?>
</div>
<?php get_footer() ?>
