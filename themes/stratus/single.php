<?php
//
// The Post page template
//

// TODO DRY ~ page

get_header();
?>
<!--single.php-->
<div class="page">
  <?php
  if (have_posts()) :
    the_post();

    get_template_part('templates/modules/banners/post-banner');
    get_template_part('templates/modules/content/post-content');
    get_template_part('templates/modules/banners/previous-post-banner');
  else:
    get_template_part('templates/modules/content/not-found');
  endif;
  ?>
</div>
<?php get_footer() ?>
