<?php
//
// The Front (Home) page template
//

get_header();
?>

<!--front-page.php-->
<div class="page">
  <?php
  get_template_part('templates/modules/banners/page-banner');
  get_template_part('templates/loops/sections');
  ?>
</div>
<?php get_footer() ?>
<!--/front-page.php-->
