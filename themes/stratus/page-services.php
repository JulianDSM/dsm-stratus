<?php
//
// Template Name: Services page
//

get_header();

?>
<!--page-services.php-->
<div class="page">
  <?php
  get_template_part('templates/modules/banners/page-banner');
  get_template_part('templates/loops/services');
  get_template_part('templates/loops/sections');
  ?>
</div>
<?php get_footer() ?>
