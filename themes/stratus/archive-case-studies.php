<?php
//
// The Case Studies Archive template
//

get_header();
?>
<div class="page">
  <?php get_template_part('templates/modules/banners/case-studies-banner') ?>

  <section class="page-section">
    <div class="grid-layout js-grid">
      <span class="grid-sizer size-third"></span>
      <?php
      if (have_posts()) :
        while (have_posts()) :
          the_post();
          ?>
          <div class="grid-item size-third">
            <?php get_template_part('templates/modules/cards/post-card') ?>
          </div>
        <?php
        endwhile;
      else :
        get_template_part('templates/modules/content/not-found');
      endif;
      ?>
    </div>
    <?php get_template_part('templates/modules/nav/posts-nav') ?>
  </section>
</div>
<?php get_footer() ?>