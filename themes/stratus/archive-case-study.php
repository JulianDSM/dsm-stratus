<?php
//
// The Case Studies Archive template
//

get_header();
?>
<div class="page">

  <header class="banner size-4:1">
    <div class="tile size-fill">
      <picture class="tile-background">
        <img src="<?php theme_images() ?>/temp_bg_I.jpg" alt="">
      </picture>

      <div class="content tile-content theme-white">
        <div class="inner-padding">
          <h1 class="h2">Case Studies</h1>
        </div>
      </div>
    </div>
  </header>

  <section id="services">
    <div class="content">
      <div class="grid-layout">
        <div class="grid-item size-one-third">
          <div class="inner-padding">
            <aside class="page-sidebar">
              <h3 class="h3">Topics</h3>

              <?php get_template_part('templates/modules/nav/categories-nav') ?>
            </aside>
          </div>
        </div>
        <div class="grid-item size-two-thirds">
          <div class="grid-layout js-grid">
            <span class="grid-sizer size-half"></span>
            <?php
            if (have_posts()) :
              while (have_posts()) :
                the_post();
                ?>
                <div class="grid-item size-half">
                  <?php get_template_part('templates/modules/cards/post-card') ?>
                </div>
              <?php
              endwhile;
            else :
              get_template_part('templates/modules/content/not-found');
            endif;
            ?>
          </div>
          <?php get_template_part('templates/modules/nav/infinite-nav') ?>
        </div>
      </div>
    </div>
  </section>
</div>
<?php get_footer() ?>