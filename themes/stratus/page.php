<?php
//
// The Page template
//

// TODO DRY ~ post

get_header();
?>
<!---page.php-->
<div class="page">
  <?php
  if (have_posts()) :
    the_post();

    get_template_part('templates/modules/banners/page-banner');
    get_template_part('templates/modules/content/content');
  else:
    get_template_part('templates/modules/content/not-found');
  endif;
  ?>
</div>
<?php get_footer() ?>
