//
// Infinite loading
//

$(function() {

  $.fn.extend({
    infiniteLoad: function(link) {

      // get page #
      var url = $('.js-infinite-loading').attr('href');
      var page_number = parseInt(url.substring(url.indexOf('/page/') + 6));

      $.ajax({
        url: app.ajax_url, // *
        type: 'post',
        data: {
          action: 'ajax_pagination',
          query_vars: app.query_vars,
          page: page_number
        },
        beforeSend: function() {
          link.addClass('is-loading');
        },
        success: function(html) {

          var $content = $(html); // NOTE appending isotope requires content as object, not string
          var $container = $('.js-infinite-loading-container');

          // remove pagination link
          $container.isotope('remove', link.closest('.grid-item'));

          // add
          $container.append($content);

          // layout
          var $grid = $container.imagesLoaded(function() {
            $grid.isotope('appended', $content);
          });
        }
      });
    }
  });

  //
  $(document).on('click', '.js-infinite-loading', function(event) {
    event.preventDefault();
  });

  //
  $(document).on('inview', '.js-infinite-loading', function(event, isInView) {

    if (isInView && !$(this).hasClass('is-loading')) {
      $.fn.infiniteLoad($(this));
    }
  });
});