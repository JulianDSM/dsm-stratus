//
// Back to top button
//

$(function() {

  // Generate a back-to-top button
  
  var isDocumentTall = $(document).height() > $(window).height() * 2;

  if (isDocumentTall) {

    // Button template
    var buttonHTML = '<a href="#top" class="blue-icon-button back-to-top-button" title="Back to top"><span class="icon arrow-up-white-icon"></span></a>';

    // Add button
    $('.site-footer').append(buttonHTML);

    // Show / hide button
    $('body').waypoint(function() {
      $('.back-to-top-button').toggleClass('is-visible');
    }, {
      offset: 'bottom-in-view'
    });

    // Smooth-scroll link
    $(document).on('click', '.back-to-top-button', function() {
      $.smoothScroll({
        scrollTarget: '#top' // ~ top
      });
      return false;
    });    
  }
});