//
// Smooth scrolling
//

$(function() {
  
  // smooth scrolling links
  $('.js-smooth-scroll').smoothScroll({
    offset: -56
  });

  // ~ smooth scroll to section on load
  var hash = window.location.hash;
  if (hash) {

    $.smoothScroll({
      scrollTarget: hash,
      offset: -56
    });
  }
});