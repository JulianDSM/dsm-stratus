//
// Site header
//

$(function() {
  
  // Sticky header
  $(window).on('scroll', function() {
    if ($(window).scrollTop() > 50) {
      $('.site-header.theme-transparent').addClass('theme-color'); // TODO r/n
    } else {
      $('.site-header.theme-transparent').removeClass('theme-color');
    }
  });
});