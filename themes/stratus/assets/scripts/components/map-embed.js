//
// Map embed
//

$(function() {

  // Escape scroll-trapping
  $('.map-embed').click(function () {
    $('.map-embed iframe').css('pointer-events', 'auto');
  });

  $('.map-embed').mouseleave(function() {
    $('.map-embed iframe').css('pointer-events', 'none'); 
  });
});