//
// Filter
//

$(function() {

  $.fn.extend({
    initGrid: function(grid) {

      // args
      var args = {
        itemSelector: '.grid-item',
        layout: 'masonry',
        //percentPosition: true,
        masonry: {
          columnWidth: '.grid-sizer',
        }
      }

      // layout
      var $grid = grid.imagesLoaded( function() {

        // Init.
        $grid.isotope(args);
      });
    },
    initGrids: function() {
      $('.js-grid').each(function() {
        $.fn.initGrid($(this));
      });
    }
  });

  // Initialise
  $.fn.initGrids();

  // non-JS grids receive matchHeight
  $('.js-grid-match-height').find('.grid-item').matchHeight();
});