//
// Sticky content
// via sticky-kit.js
//

$(function() {

  var stickyArgs = {
    'sticky_class': 'is-stuck',
    'offset_top': 56 // ~ site-header height
  }

  // Sticky content on larger displays
  if ($(window).width() >= 1280) {
    $('.js-sticky').stick_in_parent(stickyArgs);
  }

  // Enable / disable stickying above / below mobile
  $(window).resize(function() {
    if ($(this).width() >= 1280) {
      $('.js-sticky').stick_in_parent(stickyArgs);
    } else {
      $('.js-sticky').trigger('sticky_kit:detach');
    }
  });
});