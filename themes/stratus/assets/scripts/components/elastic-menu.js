//
// Filters
//

$(function() {

  // Filter button
  $(document).on('click', '.menu-item-has-children > a', function(event) {
    event.preventDefault();

    // ~ mobile
    if ($(window).width() < 1280) {

      // ~ open
      var $menuItem = $(this).closest('.menu-item-has-children');
      $menuItem.toggleClass('is-open');
      $menuItem.find('.sub-menu').slideToggle({
        duration: 250
      });

      // ~ close others
      var $menu = $(this).closest('.elastic-menu');
      var $otherMenuItems = $menu.find('.menu-item-has-children.is-open').not($menuItem);
      $otherMenuItems.removeClass('is-open');
      $otherMenuItems.find('.sub-menu').slideUp({
        duration: 250
      });
    }
  });
});