//
// Drawer
//

$(function() {

  $.fn.extend({
    openDrawer: function(drawer) {

      drawer.addClass('is-open');
      $.fn.showOverlay();

      // Close other open drawers
      $('.drawer').not(drawer).removeClass('is-open');
    },
    closeDrawer: function(drawer) {

      drawer.removeClass('is-open');
      $.fn.hideOverlay();
    },
    closeDrawers: function() {

      // Close all drawers
      $('.drawer, .main-nav').each(function() { // :P
        $.fn.closeDrawer($(this));
      });
    },
    toggleDrawer: function(drawer) {
      if (drawer.is('.is-open')) {
        $.fn.closeDrawer(drawer);
      } else {
        $.fn.openDrawer(drawer);
      }
    }
  });

  // Clicking drawer button...
  $(document).on('click', '.js-drawer-button', function(event) {
    event.preventDefault();

    var drawerSelector = '.' + $(this).data('drawer');
    var $drawer = $(drawerSelector);

    // Open / close drawer
    $.fn.toggleDrawer($drawer);
  });

  // Tapping content overlay...
  $(document).on('click', '.content-overlay', function(){

    // Close all drawers
    $.fn.closeDrawers();
  });
});