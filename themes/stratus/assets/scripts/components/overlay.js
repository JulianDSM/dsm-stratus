//
// Overlay
//

$(function() {

  $.fn.extend({
    showOverlay: function() {
      if ($('.content-overlay').length == 0) {
        $('main').prepend('<span class="content-overlay"></span>');
        $('body').addClass('is-scroll-locked');
      }
    },
    hideOverlay: function() {
      $('.content-overlay').remove();
      $('body').removeClass('is-scroll-locked');
    }
  });

  // Tapping content overlay...
  $(document).on('click', '.content-overlay', function(){

    $.fn.hideOverlay();
    $.fn.closeDrawers();
  });
});