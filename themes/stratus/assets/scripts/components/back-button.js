//
// Back button
//

$(function() {

  $(document).on('click', '.js-back-button', function(event) {
    event.preventDefault();

    // ←
    window.history.back();
  });

});

