<?php
//
// Theme setup
//

// Add theme support
add_action('after_setup_theme', 'theme_setup');
if (!function_exists('theme_setup_theme')) {
  function theme_setup() {

    // Title
    add_theme_support('title-tag');

    // HTML5
    add_theme_support('html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ));

    // Thumbnails
    remove_theme_support('post-thumbnails');
    add_theme_support('post-thumbnails', array('post', 'case-studies'));

    add_image_size('640x', 640, 9999);
  }
}

// Change the WordPress login page link
add_filter('login_headerurl', 'theme_login_logo_url');
function theme_login_logo_url() {
  return get_bloginfo('url');
}

// Customise the WordPress login page image
add_action('login_head', 'theme_login_logo');
function theme_login_logo() {
  echo "<style>
    body.login #login h1 a {
      margin: 0 auto 40px;
      width: 100%;
      height: 43px;
      background: url('" . get_bloginfo('template_url') . "/assets/images/logos/logo_stratus_wordpress-login.png') no-repeat scroll center top transparent;
    }
  </style>";
}

// Register navigation menus
add_action( 'after_setup_theme', 'theme_register_menu' );
function theme_register_menu() {
  register_nav_menu('header', 'Header');
  register_nav_menu('footer', 'Footer');
}

// Remove "Howdy" from the WordPress admin
add_filter('gettext', 'theme_remove_howdy', 10, 2);
function theme_remove_howdy($new, $original) {
  if ('Howdy, %1$s' == $original)
    $new = '%1$s';
  return $new;
}

// Register the "Case Studies" Custom Post Type
function register_case_studies() {

  $labels = array(
    'name'                  => 'Case Studies',
    'singular_name'         => 'Case Study',
    'menu_name'             => 'Case Studies',
    'name_admin_bar'        => 'Case Study',
    'archives'              => 'Case Study Archives',
    'attributes'            => 'Case Study Attributes',
    'parent_item_colon'     => 'Parent Case Study:',
    'all_items'             => 'All Case Studies',
    'add_new_item'          => 'Add New Case Study',
    'add_new'               => 'Add New',
    'new_item'              => 'New Case Study',
    'edit_item'             => 'Edit Case Study',
    'update_item'           => 'Update Case Study',
    'view_item'             => 'View Case Study',
    'view_items'            => 'View Case Studies',
    'search_items'          => 'Search Case Study',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into Case Study',
    'uploaded_to_this_item' => 'Uploaded to this Case Study',
    'items_list'            => 'Case Studies list',
    'items_list_navigation' => 'Case Studies list navigation',
    'filter_items_list'     => 'Filter Case Studies list',
  );
  $args = array(
    'label'                 => 'Case Study',
    'labels'                => $labels,
    'supports'              => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-admin-page',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,    
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type('case-studies', $args);

}
add_action('init', 'register_case_studies', 0);

// Hide the WordPress admin bar
add_filter('show_admin_bar', '__return_false');

// Streamline <head>
add_action('init', 'theme_cleanup_head');
function theme_cleanup_head() {
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'wp_generator');
  remove_action('wp_head', 'feed_links_extra', 3);
  remove_action('wp_head', 'feed_links', 2);
  remove_action('wp_head', 'index_rel_link');
  remove_action('wp_head', 'parent_post_rel_link', 10, 0);
  remove_action('wp_head', 'start_post_rel_link', 10, 0);
  remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
  remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
  wp_deregister_script('l10n');

  // Streamline emoji
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_action('admin_print_styles', 'print_emoji_styles');  
  remove_filter('the_content_feed', 'wp_staticize_emoji');
  remove_filter('comment_text_rss', 'wp_staticize_emoji');  
  remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
  add_filter('tiny_mce_plugins', 'theme_remove_tinymce_emoji');

  // Streamline REST API
  remove_action('wp_head', 'rest_output_link_wp_head', 10);
  remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
}

// Streamline TinyMCE emojis
function theme_remove_tinymce_emoji($plugins) {
  if (is_array($plugins)) {
    return array_diff($plugins, array('wpemoji'));
  } else {
    return array();
  }
}

// Add favicon
add_action('wp_head', 'theme_favicon');
function theme_favicon() {
  echo '<link rel="icon" href="' . get_template_directory_uri() . '/assets/images/icons/icon_favicon.png">';
}

// Add theme stylesheet
add_action('wp_enqueue_scripts', 'theme_stylesheet');
function theme_stylesheet() {

  // Add Google Fonts
  wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,700|Raleway:400,700', false);

  // Add theme stylesheet
  wp_enqueue_style('style', get_stylesheet_uri());
}

// Add theme scripts
add_action('wp_enqueue_scripts', 'theme_scripts');
function theme_scripts() {

  // Enqueue JavaScript
  if (!is_admin()) {
    
    // Streamline
    wp_deregister_script('wp-embed');

    // Load jQuery from Google CDN
    wp_deregister_script('jquery');
    wp_register_script('jquery', ('//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'), false, '2.2.4', false);
    wp_enqueue_script('jquery');

    // Add theme script
    wp_register_script('app', get_template_directory_uri() . '/assets/scripts/script-min.js', array('jquery'), '', true);
    wp_enqueue_script('app');
  }

  // Localize script

  global $wp_query;

  wp_localize_script('app', 'app', array(
    'ajax_url' => admin_url('admin-ajax.php'),
    'query_vars' => json_encode($wp_query->query),
    'site_url' => site_url(),
    'assets_url' => get_template_directory_uri() . '/assets'
  ));
}

// Streamline body class
add_filter('body_class', 'theme_body_class');
function theme_body_class($classes) {
  global $post;
  if (isset($post))
    $classes = array($post->post_name . '-' . $post->post_type);
  return $classes;
}

// Shortcut to theme images
function theme_images() {
  echo get_template_directory_uri() . '/assets/images';
}

// Enable editing menus for WordPress Editor-level users
$role_object = get_role('editor');
$role_object->add_cap('edit_theme_options');

// Handleize
function theme_handleize($string) {

  // lower case
  $string = strtolower($string);

  // Alphanumeric
  $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);

  // Adjacent dashes & whitespace
  $string = preg_replace("/[\s-]+/", " ", $string);

  // Convert whitespace & underscores to dashes
  $string = preg_replace("/[\s_]/", "-", $string);

  return $string;
}

// Truncate post excerpts at a custom # of characters
add_filter('excerpt_length', 'theme_excerpt_length');
function theme_excerpt_length($length) {
  return 32;
}

// Append an ellipsis to post excerpts
add_filter('excerpt_more', 'theme_excerpt_more');
function theme_excerpt_more($more) {
  return ' <span class="pseudo-ellipsis">. . .</span>'; // NOTE using Raleway, the ellipsis appears badly anti-aliased
}

// Load Gravity Forms script in the footer to resolve a jQuery conflict
add_filter('gform_init_scripts_footer', '__return_true');

// Change default Gravity Forms validation message
add_filter( 'gform_validation_message', 'theme_gravity_forms_validation_message', 10, 2 );
function theme_gravity_forms_validation_message( $message, $form ) {
  return false;
}

// AJAX post loading
add_action('wp_ajax_nopriv_ajax_pagination', 'theme_ajax_pagination');
add_action('wp_ajax_ajax_pagination', 'theme_ajax_pagination');

function theme_ajax_pagination() {

  $query_vars = json_decode(stripslashes($_POST['query_vars'] ), true);
  $query_vars['paged'] = $_POST['page'];

  $posts = new WP_Query($query_vars);
  $GLOBALS['wp_query'] = $posts;

  if ($posts->have_posts()) :
    while ($posts->have_posts()) :
      $posts->the_post();
      ?>
      <div class="grid-item size-half">
        <?php get_template_part('templates/modules/cards/post-card') ?>
      </div>
    <?php
    endwhile;
  else :
    //
  endif;

  the_posts_pagination(array(
    'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
    'next_text'          => __( 'Next page', 'twentyfifteen' ),
    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
  ));
  
  die();
}
?>