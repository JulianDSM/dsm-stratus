<?php
//
// The Category archive page template
//

// TODO DRY
get_header();
?>
<div class="page">
  <?php get_template_part('templates/modules/banners/blog-banner') ?>

  <section id="services">
    <div class="grid-layout">
      <div class="grid-item size-one-third">
        <aside class="page-sidebar">
          <div class="content has-padding">
            <h3 class="h3">Topics</h3>

            <?php get_template_part('templates/modules/nav/categories-nav') ?>
          </div>
        </aside>
      </div>
      <div class="grid-item size-two-thirds">
        <div class="grid-layout js-grid">
          <span class="grid-sizer size-half"></span>
          <?php
          if (have_posts()) :
            while (have_posts()) :
              the_post();
              ?>
              <div class="grid-item size-half">
                <?php get_template_part('templates/modules/cards/post-card') ?>
              </div>
            <?php
            endwhile;
          else :
            get_template_part('templates/modules/content/not-found');
          endif;
          ?>
        </div>
        <?php get_template_part('templates/modules/nav/posts-nav') ?>
      </div>
    </div>
  </section>
</div>
<?php get_footer() ?>