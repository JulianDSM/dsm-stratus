<!--loops/sections.php-->
<?php
//
// The Sections (ACF) loop
//

if (have_rows('sections')) :
  while (have_rows('sections')) :
    the_row();

    $id = get_sub_field('section-id');
    ?>
    <section<?php if ($id) : echo ' id="' . esc_attr($id) . '"'; endif ?> class="page-section">
      <?php
      switch (get_row_layout()) :
        case 'content' :
          get_template_part('templates/modules/content/content-dynamic');

          break;
        case 'image' :
          get_template_part('templates/modules/content/content-image');

          break;
        case 'split' :
          get_template_part('templates/modules/content/content-split');

          break;
        case 'map' :
          get_template_part('templates/modules/embeds/map-embed');

          break;
      endswitch;
      ?>
    </section>
  <?php
  endwhile;
endif;
?>
