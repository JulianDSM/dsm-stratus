<?php
//
// The Services (ACF) loop
//
?>
<!--loops/services.php-->
<section class="page-section">
  <div class="grid-layout js-grid-match-height">
    <div class="grid-item has-sidebar-border size-one-third">
      <?php get_template_part('templates/modules/sidebars/services-sidebar') ?>
    </div>
    <div class="grid-item size-two-thirds">
      <?php
      if (have_rows('services')) :

        $index = 0;
        $count = count(get_field('services'));

        while (have_rows('services')) :
          the_row();

          $index++;
          $title = get_sub_field('title');
          $slug = sanitize_title($title);
          $description = get_sub_field('description');
          ?>
          <section id="<?php echo $slug ?>" class="services-section">
            <div class="content has-padding">
              <h3 class="h3"><?php echo $title ?></h3>
              <?php echo $description ?>
            </div>

            <?php if ($index != $count) : ?>
              <hr class="section-separator">
            <?php endif ?>
          </section>
        <?php
        endwhile;
        reset_rows();
      endif;
      ?>
    </div>
  </div>
</section>
