<?php
//
// Case Study sidebar
//
?>
<aside class="case-study-sidebar js-sticky">
  <div class="content sidebar-content has-condensed-padding theme-extra-light-grey">
    <?php get_template_part('templates/modules/content/sidebar-content') ?>
  </div>
</aside>