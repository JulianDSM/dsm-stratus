<?php
//
// Case Study sidebar (A)
//
?>
<aside class="case-study-sidebar js-sticky">
  <div class="content sidebar-content has-condensed-padding">
    <?php get_template_part('templates/modules/content/case-study-meta') ?>
  </div>
</aside>