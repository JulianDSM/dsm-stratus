<?php
//
// Services sidebar
//
?>
<aside class="page-sidebar js-sticky">
  <div class="content has-padding">
    <h3 class="h3">Services</h3>

    <?php if (have_rows('services')) : ?>
      <nav class="categories-nav">
        <ol class="list">
          <?php
          while (have_rows('services')) :
            the_row();

            $title = get_sub_field('title');
            $slug = sanitize_title($title);
            $description = get_sub_field('description');
            ?>
            <li>
              <a class="js-smooth-scroll" href="#<?php echo $slug ?>"><?php echo $title ?></a>
            </li>
          <?php
          endwhile;
          reset_rows();
          ?>
        </ol>
      </nav>
    <?php endif ?>
  </div>
</aside>