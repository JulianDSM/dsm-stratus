<?php
//
// Post categories sidebar
//
?>
<aside class="page-sidebar js-sticky">
  <div class="content has-condensed-padding">
    <h3 class="h3">Topics</h3>

    <?php get_template_part('templates/modules/nav/categories-nav') ?>
  </div>
</aside>