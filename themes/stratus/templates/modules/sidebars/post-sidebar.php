<?php
//
// Post sidebar
//
?>
<aside class="post-sidebar js-sticky">
  <div class="content sidebar-content has-condensed-padding theme-extra-light-grey">
    <?php
    get_template_part('templates/modules/content/author-snippet');
    get_template_part('templates/modules/links/related-posts-links');
    ?>
  </div>
</aside>