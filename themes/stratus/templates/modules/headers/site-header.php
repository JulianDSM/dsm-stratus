<?php
//
// Site header
//
?>
<!--modules/headers/site-header.php-->
<header class="site-header<?php echo (is_front_page()) ? ' theme-transparent' : '' ?>">

  <?php get_template_part('templates/modules/buttons/menu-button') ?>

  <a class="js-smooth-scroll" href="<?php echo home_url() ?>/#top" title=""><h1 class="stratus-logo">
    Stratus</h1></a>

  <?php get_template_part('templates/modules/nav/main-nav') ?>
</header>
<!--/ modules/headers/site-header.php-->
