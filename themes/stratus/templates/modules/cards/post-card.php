<?php
//
// The Post Card template
//
?>
<article class="card post-card">
  <a class="card-link" href="<?php the_permalink() ?>">
    <?php if (has_post_thumbnail()) : ?>
      <picture class="card-media">
        <?php the_post_thumbnail('640x') ?>
      </picture>
    <?php endif ?>

    <div class="content card-content theme-white">
      <h1 class="h3"><?php the_title() ?></h1>
      <?php the_excerpt() ?>
    </div>
  </a>
</article>