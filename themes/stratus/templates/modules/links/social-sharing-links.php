<?php
//
// Social media links
//
?>
<nav class="social-media-links">
  <a class="icon-button" href="#" target="_blank">
    <span class="icon facebook-icon">Facebook</span>
  </a>

  <a class="icon-button" href="#" target="_blank">
    <span class="icon twitter-icon">Twitter</span>
  </a>

  <a class="icon-button" href="#" target="_blank">
    <span class="icon instagram-icon">Instagram</span>
  </a>
</nav>