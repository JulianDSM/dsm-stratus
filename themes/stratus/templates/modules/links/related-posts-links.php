<?php
//
// Related posts links (from post-sidebar)
//
?>
<section class="sidebar-section">
  <nav class="related-posts-links">
    <h3 class="h3">Related Posts</h3>
    
    <?php wp_related_posts() ?>
  </nav>
</section>