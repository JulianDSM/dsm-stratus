<?php
//
// Related post links (from post-sidebar)
//
?>
<section class="sidebar-section">
  <nav class="related-articles-links">
    <h3 class="h3">Related Articles</h3>
    
    <?php wp_related_posts() ?>
  </nav>
</section>