<?php
//
// The post author information snippet (from post-sidebar)
//
?>
<section class="sidebar-section">
  <h2 class="h3">Author</h2>
  <p class="author-name"><?php the_author() ?></p>
  <p class="author-description"><?php the_author_meta('description', $post->post_author) ?></p>
</section>