<?php
//
// The Post content template
//
?>
<article>
  <div class="grid-layout above-tablet js-grid-match-height">
    <div class="grid-item has-sidebar-border size-one-fifth">
      <?php get_template_part('templates/modules/sidebars/case-study-sidebar-A') ?>
    </div>
    <div class="grid-item size-three-fifths">
      <?php get_template_part('templates/modules/content/content') ?>
    </div>
    <div class="grid-item size-one-fifth">
      <?php get_template_part('templates/modules/sidebars/case-study-sidebar-B') ?>
    </div>
  </div>
</article>