<?php
//
// The Case Study meta content template
//

$client_name = get_field('case-study-client-name');
$client_logo = get_field('case-study-client-logo');
$client_website = get_field('case-study-client-website');
?>
<div class="content sidebar-content has-condensed-padding">
  <div class="sidebar-section">
    <p class="h6">Client</p>
    <?php if ($client_logo) : ?>
      <img src="<?php echo $client_logo['url'] ?>">
    <?php
    else :
      if ($client_name) :
      ?>
        <p class="h6"><?php echo $client_name ?></p>
      <?php else : ?>
        <p class="h6"><?php echo the_title() ?></p>
      <?php endif ?>
    <?php endif ?>
  </div>
  
  <div class="sidebar-section">
    <?php if ($client_website) : ?>
      <p class="h6">Website</p>
      <p><a href="<?php echo $client_website ?>" target="_blank"><?php echo $client_website ?></a></p>
    <?php endif ?>
  </div>
</div>