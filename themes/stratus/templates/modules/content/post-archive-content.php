<?php
//
// The Post archive content template
//

get_template_part('templates/modules/banners/blog-banner') ?>

<section class="page-section">
  <div class="grid-layout">
    <div class="grid-item size-one-third">
      <?php get_template_part('templates/modules/sidebars/post-categories-sidebar') ?>
    </div>
    <div class="grid-item size-two-thirds">
      <div class="grid-layout js-grid js-infinite-loading-container">
        <span class="grid-sizer size-half"></span>
        <?php
        if (have_posts()) :
          while (have_posts()) :
            the_post();
            ?>
            <div class="grid-item size-half">
              <?php get_template_part('templates/modules/cards/post-card') ?>
            </div>
          <?php
          endwhile;
        else :
          get_template_part('templates/modules/content/not-found');
        endif;
        ?>
      </div>
      <?php get_template_part('templates/modules/nav/posts-nav') ?>
    </div>
  </div>
</section>