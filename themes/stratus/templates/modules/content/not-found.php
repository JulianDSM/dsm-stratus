<?php
//
// The "content not found" ~ 404 snippet
//
?>
<div class="content has-padding">
  <h1 class="h2">Not Found</h1>
  <p>Sorry, the content you were looking for could not be found.</p>
</div>