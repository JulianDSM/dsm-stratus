<?php
//
// The split content (ACF) template
//

$split_sizing = get_sub_field('split-sizing');

switch ($split_sizing) :
  case 'two-thirds-one-third' :
    $first_element_size = 'size-two-thirds';
    $second_element_size = 'size-one-third';

    break;
  case 'one-thirds-two-thirds' :
    $first_element_size = 'size-one-third';
    $second_element_size = 'size-two-thirds';

    break;
  default :
    $first_element_size = $second_element_size = 'size-half';
endswitch;

if (have_rows('split-sections')) :
  ?>
  <div class="grid-layout js-grid-match-height">
    <?php
    $i = 0;
    while (have_rows('split-sections')) :
      the_row();

      $i++;
      $grid_item_size = ($i == 1) ? $first_element_size : $second_element_size;
      $id = get_sub_field('section-id');
      ?>
      <div<?php if ($id) : echo ' id="' . esc_attr($id) . '"'; endif ?> class="grid-item <?php echo $grid_item_size ?>">
        <?php
        switch (get_row_layout()) :
          case 'content' :
            get_template_part('templates/modules/content/content-dynamic');

            break;
          case 'image' :
            get_template_part('templates/modules/content/content-image');

            break;
        endswitch;
        ?>
      </div>
    <?php endwhile ?>
  </div>
<?php endif ?>