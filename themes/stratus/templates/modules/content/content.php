<?php
//
// Content
//
?>
<div class="content clamp-width has-padding">
  <?php the_content() ?>
</div>