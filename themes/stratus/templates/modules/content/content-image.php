<?php
//
// The image content (ACF) template
//

$image = get_sub_field('image');
$image_mobile = get_sub_field('image-mobile');

if ($image) :
  ?>
  <picture class="image-content">
    <?php if ($image_mobile) : ?>
      <source srcset="<?php echo $image['url'] ?>" media="(min-width: 720px)">
      <img srcset="<?php echo $image_mobile['url'] ?>" alt="<?php echo $image['alt'] ?>">
    <?php else : ?>
      <img srcset="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
    <?php endif ?>
  </picture>
<?php endif ?>