<?php
//
// The dynamic content (ACF) template
//

$theme = '';
$align = '';
$padding = 'has-padding';
$tile_size = '';

$background_type = get_sub_field('background-type');
$content = get_sub_field('content');
$content_alignment = get_sub_field('content-alignment');
$size = get_sub_field('size');

// background-type
switch ($background_type) :
  case 'color' :
    $background_color = get_sub_field('background-color');

    if ($background_color != 'white') :
      $theme = 'theme-' . $background_color;
    endif;

    break;
  case 'image' :
    $background_image = get_sub_field('background-image');
    $background_image_mobile = get_sub_field('background-image-mobile');
    $theme = 'theme-white';

    break;
  case 'video' :
    $theme = 'theme-white';
    $tile_size = 'size-fill';
    $video_size = get_sub_field('background-video-size');

    break;
endswitch;

// align
switch ($content_alignment) :
  case 'center' :
    $align = 'align-center';
    break;
  default :
    $align = 'align-bottom-left';
endswitch;

// size
if ($size == 'full-screen') :
  $tile_size = 'size-fill';
  $align = 'align-middle';
endif;

//
if ($background_type == 'image') :
?>
<div class="tile<?php if ($tile_size != '') : echo ' ' . $tile_size; endif; ?>">
  <picture class="tile-background">
    <?php if ($background_image_mobile) : ?>
      <source srcset="<?php echo $background_image['url'] ?>" media="(min-width: 720px)">
      <img srcset="<?php echo $background_image_mobile['url'] ?>" alt="<?php echo $background_image['alt'] ?>">
    <?php else : ?>
      <img srcset="<?php echo $background_image['url'] ?>" alt="<?php echo $background_image['alt'] ?>">
    <?php endif ?>
  </picture>

  <div class="content tile-content <?php echo $padding ?> <?php echo $align ?> theme-white">
    <?php echo $content ?>
  </div>
</div>
<?php
elseif ($background_type == 'video') :
  $mp4 = get_sub_field('background-video-mp4');
  $webm = get_sub_field('background-video-webm');
  $ogv = get_sub_field('background-video-ogv');
  $poster = get_sub_field('background-video-poster');
  ?>
    <div class="tile<?php if ($tile_size != '') : echo ' ' . $tile_size; endif; ?>">
      <video class="tile-background video-background<?php if ($video_size == 'contain') echo ' size-contain' ?>" playsinline autoplay muted loop poster="<?php echo $poster['url'] ?>">
        <source src="<?php echo $mp4['url'] ?>" type="video/mp4">
        <source src="<?php echo $webm['url'] ?>" type="video/webm">
        <source src="<?php echo $ogv['url'] ?>" type="video/ogg">
      </video>

      <div class="content tile-content <?php echo $padding ?> <?php echo $align ?> theme-white">
        <?php echo $content ?>
      </div>
    </div>

<?php else : ?>
  <div class="content <?php echo $padding ?> <?php if ($theme != '') : echo ' ' . $theme; endif ?>">
    <?php echo $content ?>
  </div>
<?php endif ?>