<?php
//
// Dynamic sidebar content
//

$content = get_field('content');

if ($content) :
?>
<div class="content sidebar-content has-condensed-padding theme-extra-light-grey">
  <?php echo $content ?>
</div>
<?php endif ?>