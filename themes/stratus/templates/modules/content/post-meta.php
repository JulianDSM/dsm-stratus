<?php
//
// Post meta
//
?>
<aside class="post-meta" role="contentinfo">
  <div class="content has-padding align-center">
    <p><?php echo get_the_date() ?></p>
  </div>
</aside>