<?php
//
// The Blog (Posts archive + Categories) page banner template
//

if (have_rows('page-banners', get_option('page_for_posts'))) :
  while (have_rows('page-banners', get_option('page_for_posts'))) :
    the_row();

    get_template_part('templates/modules/banners/banner-part');
  endwhile;
endif;
?>