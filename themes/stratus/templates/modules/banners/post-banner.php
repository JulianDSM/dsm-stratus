<!--post-banner.php-->
<?php
//
// The post banner template
//

// ref. DRY page-banner
if (have_rows('page-banners')) :
  while (have_rows('page-banners')) :
    the_row();
    ?>
    <header class="banner">
      <?php
      $theme = '';
      $background_type = get_sub_field('background-type');

      switch ($background_type) :
        case 'color' :
          $background_color = get_sub_field('background-color');

          if ($background_color != 'white')
            $theme = 'theme-' . $background_color;

          break;
        case 'image' :
          $background_image = get_sub_field('background-image');
          $background_image_mobile = get_sub_field('background-image-mobile');
          $theme = 'theme-white';

          break;
        case 'video' :
          $theme = 'theme-white';

          break;
      endswitch;

      if ($background_type == 'image') :
      ?>
      <div class="tile<?php if ($tile_size != '') : echo ' ' . $tile_size; endif; ?>">
        <picture class="tile-background">
          <?php if ($background_image_mobile) : ?>
            <source srcset="<?php echo $background_image['url'] ?>" media="(min-width: 720px)">
            <img srcset="<?php echo $background_image_mobile['url'] ?>" alt="<?php echo $background_image['alt'] ?>">
          <?php else : ?>
            <img srcset="<?php echo $background_image['url'] ?>" alt="<?php echo $background_image['alt'] ?>">
          <?php endif ?>
        </picture>

        <div class="content tile-content has-padding align-center theme-white">
          <p class="h6"><?php echo get_the_date() ?></p>
          <h1 class="h2"><?php the_title() ?></h1>
        </div>
      </div>
      <?php
      elseif ($background_type == 'video') :
        $mp4 = get_sub_field('background-video-mp4');
        $webm = get_sub_field('background-video-webm');
        $ogv = get_sub_field('background-video-ogv');
        ?>
          <div class="tile">
            <video class="tile-background video-background" playsinline autoplay muted loop>
              <source src="<?php echo $mp4['url'] ?>" type="video/mp4">
              <source src="<?php echo $webm['url'] ?>" type="video/webm">
              <source src="<?php echo $ogv['url'] ?>" type="video/ogg">
            </video>

            <div class="content tile-content has-padding align-center theme-white">
              <p class="h6"><?php echo get_the_date() ?></p>
              <h1 class="h2"><?php the_title() ?></h1>
            </div>
          </div>

      <?php else : ?>
        <div class="content has-padding align-center <?php if ($theme != '') : echo ' ' . $theme; endif ?>">
          <p class="h6"><?php echo get_the_date() ?></p>
          <h1 class="h2"><?php the_title() ?></h1>
        </div>
      <?php endif ?>
    </header>
    <?php
  endwhile;
endif;
?>
