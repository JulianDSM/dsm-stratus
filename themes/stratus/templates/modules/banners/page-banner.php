<!--page-banner.php-->
<?php
//
// The general page banner template
//

if (have_rows('page-banners')) :
  while (have_rows('page-banners')) :
    the_row();

    get_template_part('templates/modules/banners/banner-part');
  endwhile;
endif;
?>
