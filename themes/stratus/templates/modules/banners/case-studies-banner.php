<?php
//
// The "Case Studies" page banner template
//

if (have_rows('page-banners', get_page_by_path('case-studies'))) :
  while (have_rows('page-banners', get_page_by_path('case-studies'))) :
    the_row();

    get_template_part('templates/modules/banners/banner-part');
  endwhile;
endif;
?>