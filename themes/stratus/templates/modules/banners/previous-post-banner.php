<?php
//
// The previous post banner template
//

global $post;
$previous_post = get_previous_post();

if ($previous_post) :
  $post = $previous_post;
  setup_postdata($post);
  ?>
  <a class="previous-post-link" href="<?php the_permalink() ?>" title="<?php esc_attr(the_title()) ?>">
    <div class="content has-padding align-center theme-dark-blue">
      <p class="h6">Previous Post</p>
      <h1 class="h3"><?php the_title() ?></h1>
      <button class="button photo-overlay-button"><span class="button-label">Read</span></button>
    </div>
  </a>
  <?php
  wp_reset_postdata();
endif;
?>