<!--home-page-banner.php-->

<?php
//
// Home page banner
//

if (have_rows('home-page-banners')) :
  while (have_rows('home-page-banners')) :
    the_row();
    $content = get_sub_field('content');
    ?>
    <header id="intro" class="page-header">
      <div class="cover-banner">
        <div class="tile size-fill">
          <video class="tile-background video-background" playsinline autoplay muted loop>
            <source src="<?php echo get_template_directory_uri() ?>/assets/videos/clouds.mp4" type="video/mp4">
            <source src="<?php echo get_template_directory_uri() ?>/assets/videos/clouds.webm" type="video/webm">
            <source src="<?php echo get_template_directory_uri() ?>/assets/videos/clouds.ogv" type="video/ogg">
          </video>

          <div class="content tile-content align-middle theme-white">
            <?php echo $content ?>

            <div class="button-group">
              <a class="button photo-overlay-button connecting-line-detail anim-floating js-smooth-scroll" href="#about">
                <span class="button-label">Learn More</span>
                <span class="icon small-arrow-circle-down-white-icon"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
  <?php
  endwhile;
endif;
?>
