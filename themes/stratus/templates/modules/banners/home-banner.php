<?php
//
// Home page banner
//
?>

<div class="cover-banner">
  <div class="tile size-fill">
    <video class="tile-background video-background" playsinline autoplay muted loop poster="<?php echo get_template_directory_uri() ?>/assets/videos/clouds-poster.jpg">
      <source src="<?php echo get_template_directory_uri() ?>/assets/videos/clouds.mp4" type="video/mp4">
      <source src="<?php echo get_template_directory_uri() ?>/assets/videos/clouds.webm" type="video/webm">
      <source src="<?php echo get_template_directory_uri() ?>/assets/videos/clouds.ogv" type="video/ogg">
    </video>

    <div class="content tile-content align-middle theme-white">
      <p class="h5">Welcome to Stratus</p>
      <h1 class="h1">A Search First Digital Agency</h1>
      <p class="h4">Elevate your online positioning</p>

      <div class="button-group">
        <a class="button photo-overlay-button connecting-line-detail anim-floating js-smooth-scroll" href="#about">
          <span class="button-label">Learn More</span>
          <span class="icon small-arrow-circle-down-white-icon"></span>
        </a>
      </div>
    </div>
  </div>
</div>