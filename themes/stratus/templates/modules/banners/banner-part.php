<?php
//
// A partial banner template, see page-banner etc.
//

$size = get_sub_field('size');

if ($size == 'full-screen') :
  $banner_type = 'cover-banner';
else :
  $banner_type = 'banner';
endif;
?>
<header class="<?php echo $banner_type ?>">
  <?php get_template_part('templates/modules/content/content-dynamic') ?>
</header>