<?php
//
// Site footer
//
?>
<!--templates/modules/footers/site-footer.php-->
<footer class="site-footer">
  <div class="content has-padding theme-dark-navy">
    <?php get_template_part('templates/modules/nav/sitemap-nav') ?>
    
  </div>
</footer>
