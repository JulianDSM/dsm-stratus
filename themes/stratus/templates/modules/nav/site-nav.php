<?php
//
// Site nav
//
?>
<!--modules/nav/site-nav.php-->
<nav class="site-nav" role="navigation">
  <ul class="menu dropdown-menu theme-white"><!-- refactor site-nav menu to include dropdown-menu obj. above mobile breakpoints -->
    <?php
    $args = array(
      'container'  => false,
      'theme_location' => 'header',
      'items_wrap' => '%3$s',
    );
    wp_nav_menu($args);
    ?>
  </ul>
</nav>
<!--/ modules/nav/site-nav.php-->
