<?php
//
// Main nav
//
?>
<nav class="main-nav" role="navigation">
  <ul class="menu main-menu theme-white">
    <?php
    $args = array(
      'container'  => false,
      'theme_location' => 'header',
      'items_wrap' => '%3$s',
    );
    wp_nav_menu($args);
    ?>
  </ul>
</nav>