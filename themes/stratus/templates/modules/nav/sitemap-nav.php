<?php
//
// Site nav
//
?>
<!--templates/modules/footers/sitemap-nav.php-->
<nav role="navigation">
  <ul class="menu sitemap-menu theme-white">
    <?php
    $args = array(
      'container'  => false,
      'theme_location' => 'footer',
      'items_wrap' => '%3$s',
    );
    wp_nav_menu($args);
    ?>
      <a href="<?php the_field('footer_link_google', 4182); ?>"><img class="footer_image_link" src="<?php the_field('footer_image_google', 4182); ?>" alt="Google Partner" /></a>
      <a href="<?php the_field('footer_link_bing', 4182); ?>"><img class="footer_image_link" src="<?php the_field('footer_image_bing', 4182); ?>" alt="Bing Accredited Professional" /></a>

  </ul>

</nav>
