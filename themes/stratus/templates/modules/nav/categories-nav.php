<?php
//
// (Post) Categories navigation links
//
?>
<nav class="categories-nav">
  <ol class="list">
    <?php
    $current_category = get_queried_object();
    $current_category_ID = $current_category->term_id;

    $args = array(
      'orderby' => 'name',
      'order'  => 'ASC'
    );

    $categories = get_categories($args);

    foreach ($categories as $category) :
      $category_url = esc_url(get_category_link($category->term_id));
      ?>
      <li>
        <a<?php if ($category->term_id == $current_category_ID) : ?> class="is-current"<?php endif ?> href="<?php echo $category_url ?>"><?php echo $category->name ?></a>
      </li>
    <?php endforeach ?>
  </ol>
</nav>