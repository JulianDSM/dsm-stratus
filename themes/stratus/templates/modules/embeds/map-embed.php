<?php
//
// An embedded Google Map template
//
?>
<aside class="map-embed">
  <div id="map"></div>
  <script>
    function initMap() {

      var myCoordinates = { lat: 43.658095, lng: -79.350138 };
      
      var map = new google.maps.Map(document.getElementById('map'), {
        center: myCoordinates,
        scrollwheel: false,
        zoom: 14,
        mapTypeControl: false,
        streetViewControl: false,
        disableDefaultUI: true,
        styles: [
          {
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#212121"
              }
            ]
          },
          {
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#212121"
              }
            ]
          },
          {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "administrative.country",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          },
          {
            "featureType": "administrative.land_parcel",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "administrative.locality",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#bdbdbd"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#181818"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#1b1b1b"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#2c2c2c"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#8a8a8a"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#373737"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#3c3c3c"
              }
            ]
          },
          {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#4e4e4e"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "featureType": "transit",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#000000"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#3d3d3d"
              }
            ]
          }
        ]
      });

      var contentString = '<h2>Stratus</h2>' +
        '<p>100 Broadview Ave, Suite 300<br>' +
        'Toronto, Canada</p>' +
        '<p><a href="https://goo.gl/maps/NiaFXeuSwBr" target="_blank" style="color: #99bfec; text-decoration: underline;">Open in Google Maps</a></p>';

      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });

      var icon = 'http://dev-stratus.pantheonsite.io/wp-content/themes/stratus/assets/images/icons/icon_map-marker.png';

      var marker = new google.maps.Marker({
        position: myCoordinates,
        map: map,
        title: 'Stratus',
        icon: icon
      });

      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });

      marker.setMap(map);
    }

  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKCyBqaTrdE-tTBk5oka38B-3ucA_6tso&callback=initMap"
  async defer></script>
</aside>