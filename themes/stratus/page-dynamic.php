<?php
//
// Template Name: Custom page
//

get_header();
?>
<!--page-dynamic.php-->
<div class="page">
  <?php
  get_template_part('templates/modules/banners/page-banner');
  get_template_part('templates/loops/sections');
  ?>
</div>
<?php get_footer() ?>
