<?php
//
// The Case Study post template
//

get_header();
?>
<!--single-case-study-->
<div class="page">
  <?php
  if (have_posts()) :
    the_post();

    get_template_part('templates/modules/banners/post-banner');
    get_template_part('templates/modules/content/case-study-content');
  else:
    get_template_part('templates/modules/content/not-found');
  endif;
  ?>
</div>
<?php get_footer() ?>
