<?php

/**
 * Description of RM_Field_Factory
 *
 */
class RM_Field_Factory {
    
    protected $db_field;
    protected $field_name;
    protected $field_options;
    protected $gopts;
    protected $opts;
    protected $x_opts;
            
    public function __construct($db_field,$opts){
        $this->db_field= $db_field;
        $this->gopts= new RM_Options;
        $this->opts= $opts;
        $this->field_options = maybe_unserialize($db_field->field_options);
        $this->db_field->field_value = maybe_unserialize($db_field->field_value);
        $this->field_name= $db_field->field_type."_".$db_field->field_id;
        
        if(isset($this->field_options->icon))
            $this->x_opts = (object)array('icon' => $this->field_options->icon);
        else
            $this->x_opts = null;
        
    }
    
    //Profile fields with pre-filled values for currently logged in user
    public function create_fname_field(){
        if(is_user_logged_in() && (!isset($this->opts['value']) || !$this->opts['value']))
        {
            $current_user = wp_get_current_user();  
            $user_fname= get_user_meta($current_user->ID, 'first_name', true);
            $this->opts['value'] = ($user_fname == '')? null : $user_fname;
        }
       return new RM_Frontend_Field_Base($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
   
     }
     
     public function create_lname_field(){
        if(is_user_logged_in() && (!isset($this->opts['value']) || !$this->opts['value']))
        {
            $current_user = wp_get_current_user();  
            $user_lname= get_user_meta($current_user->ID, 'last_name', true);
            $this->opts['value'] = ($user_lname == '')? null : $user_lname;
        }
       return new RM_Frontend_Field_Base($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
   
     }
     public function create_binfo_field(){
        if(is_user_logged_in() && (!isset($this->opts['value']) || !$this->opts['value']))
        {
            $current_user = wp_get_current_user();  
            $user_binfo= get_user_meta($current_user->ID, 'description', true);
            $this->opts['value'] = ($user_binfo == '')? null : $user_binfo;
        }
       return new RM_Frontend_Field_Base($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
   
     }     
    
    public function create_price_field(){
        $currency_pos = $this->gopts->get_value_of('currency_symbol_position');
        $currency_symbol = $this->gopts->get_currency_symbol();
        return new RM_Frontend_Field_Price($this->db_field->field_id, $this->db_field->field_label,$this->field_name, $this->opts, $this->db_field->field_value, $currency_pos, $currency_symbol, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts); 
    }
    
    public function create_file_field(){
        return new RM_Frontend_Field_File($this->db_field->field_id, $this->db_field->field_label,$this->field_name, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_select_field(){
        return new RM_Frontend_Field_Select($this->db_field->field_id, $this->db_field->field_label,$this->field_name, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_MultiDropdown_field(){
        $this->opts['multiple']='multiple';
        return new RM_Frontend_Field_Select($this->db_field->field_id, $this->db_field->field_label,$this->field_name, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_multi_dropdown_field(){
        $field= $this->create_select_field();
        return $field;
    }
    
    public function create_base_field(){
        return new RM_Frontend_Field_Base($this->db_field->field_id,'Textbox',$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_phone_field(){
        $validate = new Validation_RegExp("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/",  RM_UI_Strings::get("PHONE_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$";
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_mobile_field(){
        $validate = new Validation_RegExp("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/",  RM_UI_Strings::get("MOBILE_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$";
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_nickname_field(){
      if(is_user_logged_in() && (!isset($this->opts['value']) || !$this->opts['value']))
        {
            $current_user = wp_get_current_user();  
            $user_nickname= get_user_meta($current_user->ID, 'nickname', true);
            $this->opts['value'] = ($user_nickname == '')? null : $user_nickname;
        }
       return new RM_Frontend_Field_Base($this->db_field->field_id,'Nickname',$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_image_field(){
        $this->opts['accept']="image/*";
        return $this->create_file_field();
    }
    
    public function create_facebook_field(){
        $validate = new Validation_RegExp("/(?:https?:\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*?(\/)?([\w\-\.]*)/",  RM_UI_Strings::get("FACEBOOK_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "(?:https?:\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*?(\/)?([\w\-\.]*)";
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_website_field(){
        $this->opts['Pattern'] = "((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)";
       if(is_user_logged_in() && (!isset($this->opts['value']) || !$this->opts['value']))
        {
            $current_user = wp_get_current_user(); 
            $this->opts['value'] = isset($current_user->user_url)? $current_user->user_url : null;
        }
        return new RM_Frontend_Field_Base($this->db_field->field_id,'Website',$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_twitter_field(){
        $validate = new Validation_RegExp("/(ftp|http|https):\/\/?((www|\w\w)\.)?twitter.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/",  RM_UI_Strings::get("TWITTER_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "(ftp|http|https):\/\/?((www|\w\w)\.)?twitter.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?";
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_google_field(){
        $validate = new Validation_RegExp("/((http:\/\/(plus\.google\.com\/.*|www\.google\.com\/profiles\/.*|google\.com\/profiles\/.*))|(https:\/\/(plus\.google\.com\/.*)))/i",  RM_UI_Strings::get("GOOGLE_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "((http:\/\/(plus\.google\.com\/.*|www\.google\.com\/profiles\/.*|google\.com\/profiles\/.*))|(https:\/\/(plus\.google\.com\/.*)))";
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_instagram_field(){
        $validate = new Validation_RegExp("/(?:^|[^\w])(?:@)([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)/", RM_UI_Strings::get("INSTAGRAM_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "(?:^|[^\w])(?:@)([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)";
        $field= $this->create_base_field();
        return $field;
        
    }
    
    public function create_linked_field(){
        $validate = new Validation_RegExp("/(ftp|http|https):\/\/?((www|\w\w)\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/", RM_UI_Strings::get("LINKED_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "(ftp|http|https):\/\/?((www|\w\w)\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?";
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_soundcloud_field(){
        $validate = new Validation_RegExp("/(ftp|http|https):\/\/?((www|\w\w)\.)?soundcloud.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/", RM_UI_Strings::get("SOUNDCLOUD_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "(ftp|http|https):\/\/?((www|\w\w)\.)?soundcloud.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?";
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_youtube_field(){
        $validate = new Validation_RegExp("/(ftp|http|https):\/\/?((www|\w\w)\.)?youtube.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/",  RM_UI_Strings::get("YOUTUBE_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "(ftp|http|https):\/\/?((www|\w\w)\.)?youtube.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?";
        $field= $this->create_base_field();
        return $field;
        
    }
    
    public function create_vkontacte_field(){
        $validate = new Validation_RegExp("/(ftp|http|https):\/\/?((www|\w\w)\.)?(vkontakte.com|vk.com)(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/", RM_UI_Strings::get("VKONTACTE_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "(ftp|http|https):\/\/?((www|\w\w)\.)?(vkontakte.com|vk.com)(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?";
        $field= $this->create_base_field();
        return $field;
        
    }
    
    public function create_skype_field(){
        $validate = new Validation_RegExp("/[a-zA-Z][a-zA-Z0-9_\-\,\.]{5,31}/", RM_UI_Strings::get("SKYPE_ERROR"));
        $this->opts['validation'] = $validate;
        $this->opts['Pattern'] = "[a-zA-Z][a-zA-Z0-9_\-\,\.]{5,31}";
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_bdate_field(){
        return new RM_Frontend_Field_Bdate($this->db_field->field_id,$this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_secemail_field(){
        return new RM_Frontend_Field_Base($this->db_field->field_id,'SecEmail',$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_gender_field(){
        //These option must be same as specified in gender field analytics calculation.
        $this->db_field->field_value=array("Male" => RM_UI_Strings::get("LABEL_GENDER_MALE"), "Female" => RM_UI_Strings::get("LABEL_GENDER_FEMALE"));
        $field= $this->create_radio_field();
        return $field;
    }
    
    public function create_terms_field(){
        $this->opts['cb_label'] = isset($this->field_options->tnc_cb_label)?  $this->field_options->tnc_cb_label:null;
        return new RM_Frontend_Field_Terms($this->db_field->field_id, $this->db_field->field_label,$this->field_name, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_language_field(){
        $this->db_field->field_value= RM_Utilities::get_language_array();
        return $this->create_select_field();
    }
    
    public function create_radio_field(){
        return new RM_Frontend_Field_Radio($this->db_field->field_id, $this->db_field->field_label,$this->field_name, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_checkbox_field(){
        return new RM_Frontend_Field_Checkbox($this->db_field->field_id, $this->db_field->field_label,$this->field_name, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_shortcode_field(){
        $this->db_field->field_value = do_shortcode($this->db_field->field_value );
        return new RM_Frontend_Field_Visible_Only($this->db_field->field_id,'HTMLCustomized',$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_divider_field(){
       return new RM_Frontend_Field_Visible_Only($this->db_field->field_id,'HTMLCustomized',$this->field_name, $this->db_field->field_label, $this->opts,' <hr class="rm_divider" width="100%" size="8" align="center">', $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_spacing_field(){
       return new RM_Frontend_Field_Visible_Only($this->db_field->field_id,'HTMLCustomized',$this->field_name, $this->db_field->field_label, $this->opts,'<div class="rm_spacing"></div>', $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_htmlh_field(){
        return new RM_Frontend_Field_Visible_Only($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_htmlp_field(){
        return new RM_Frontend_Field_Visible_Only($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->field_value, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_time_field(){
        return new RM_Frontend_Field_Time($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_rating_field(){
        return new RM_Frontend_Field_Rating($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_custom_field(){
        $custom_validation=null;
        if(isset($this->opts['field_validation']) && $this->opts['field_validation'] != null)
        {
            if($this->opts['field_validation']=='custom' && isset($this->opts['custom_validation']))
                $custom_validation=$this->opts['custom_validation'];
            else
                $custom_validation=$this->opts['field_validation'];

            $validate = new Validation_RegExp("/'.$custom_validation.'/", RM_UI_Strings::get("SKYPE_ERROR"));
             //$this->opts['validation'] = $validate;
             $this->opts['Pattern'] = $custom_validation; 

        }
                   
        $field = $this->create_base_field();
        return $field;
    }
    
    public function create_email_field(){
        // in this case pre-populate the primary email field with logged-in user's email.
        if($this->db_field->is_field_primary && (!isset($this->opts['value']) || !$this->opts['value']))
        {
            if(is_user_logged_in())
            {
                $current_user = wp_get_current_user();                            
                $this->opts['value'] = $current_user->user_email;
            }
        }
       return new RM_Frontend_Field_Base($this->db_field->field_id,$this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_address_field(){
        $field= $this->create_geo_field();
        return $field;
    }
    
    public function create_map_field(){
        $field= $this->create_geo_field();
        return $field;
    }
    
    public function create_geo_field(){
        $service = new RM_Front_Form_Service;
        return new RM_Frontend_Field_GGeo($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $service->get_setting('google_map_key'), $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
    public function create_textbox_field(){
        $field= $this->create_base_field();
        return $field;
    }
    
    public function create_default_field(){
        return new RM_Frontend_Field_Base($this->db_field->field_id, $this->db_field->field_type,$this->field_name, $this->db_field->field_label, $this->opts, $this->db_field->page_no, $this->db_field->is_field_primary, $this->x_opts);
    }
    
}
