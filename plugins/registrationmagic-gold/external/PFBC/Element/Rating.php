<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Map
 *
 * @author CMSHelplive
 */
class Element_Rating extends Element
{

    protected $_attributes = array(
        'type' => 'text'
    );
    
    protected $jQueryOptions = "";

    /* public function getCSSFiles()
      {
      return array(
      );
      } */

    public function __construct($label, $name, array $properties = null)
    {
        parent::__construct($label, $name, $properties);
        $this->_attributes['id'] = $name;
        
        if(isset($properties['required']))
        $this->_attributes['required']=$properties['required'];

    }

     public function getJSFiles()
    {
        return array('script_rm_rating' => RM_BASE_URL . 'public/js/rating3/jquery.rateit.js');
    }
    
   public function getCSSFiles()
    {
        return array(
            'style_rm_rating' => RM_BASE_URL . 'public/js/rating3/rateit.css' );
    }
    public function getJSDeps()
    {
        return array(
            'script_rm_rating'
        );
    }

    public function jQueryDocumentReady()
    {
        echo <<<JS
         jQuery(".rateit").on('rated', function (event, value) {
        jQuery(event.currentTarget).closest('form').find(".rateitbackend").val(value);
    });
    jQuery(".rateit").on('reset', function () {
        jQuery(event.currentTarget).closest('form').find(".rateitbackend").val(0);
    });
JS;
        
        parent::jQueryDocumentReady();
        //echo 'initMap();';
    }

    public function render()
    {   
        if(isset($this->_attributes['required']))
            $req_attr = "required";
        else
            $req_attr = "";
        
        $id = "rm_hidden_rate_".$this->_attributes['id'];
        ?>  
<div class="rateit" id="rm_rateit5" data-rateit-min="0" data-rateit-max="5" data-rateit-value="<?php echo $this->getAttribute('value'); ?>" data-rateit-ispreset="true" >
    
    <input type="hidden" class="rateitbackend <?php echo $this->getAttribute('class'); ?>" id="<?php echo $id; ?>" name="<?php echo $this->_attributes['id']; ?>" <?php echo $req_attr; ?> value="<?php echo $this->getAttribute('value'); ?>"/>
</div>

 
  <?php
    }
    
}
