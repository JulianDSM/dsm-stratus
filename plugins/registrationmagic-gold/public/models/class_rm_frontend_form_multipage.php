<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RM_Frontend_Form_Multipage extends RM_Frontend_Form_Base
{

    protected $form_pages;
    protected $ordered_form_pages;
    
    public function __construct(RM_Forms $be_form, $ignore_expiration=false)
    {
        parent::__construct($be_form, $ignore_expiration);

        if ($this->form_options->form_pages == null)
        {
            $this->form_pages = array('Page 1');
            $this->ordered_form_pages = array(0);
        }
        else
        {
            $this->form_pages = $this->form_options->form_pages;
            if ($this->form_options->ordered_form_pages == null)
                $this->ordered_form_pages = array_keys($this->form_pages);
            else
                $this->ordered_form_pages = $this->form_options->ordered_form_pages;
        }
    }

    public function get_form_pages()
    {
        return $this->form_pages;
    }

    public function pre_sub_proc($request, $params)
    {
        return true;
    }

    public function post_sub_proc($request, $params)
    {
        return true;
    }
    
    //Following two methods can be overloaded by child classes in order to add custom fields to any page of the form.
    protected function hook_pre_field_addition_to_page($form, $page_no)
    {
        
    }
    
    protected function hook_post_field_addition_to_page($form, $page_no,$editing_sub=null)
    {
        
    }

    protected function prepare_fields_for_render($form,$editing_sub=null)
    { 
        $form->addElement(new Element_Hidden("rm_tp_timezone","",array("id"=>"id_rm_tp_timezone")));     
        $n = 1; //page no(ordinal no. not actual) maintained for js traversing through pages.
        
        foreach ($this->ordered_form_pages as $fp_no)
        {   $k = intval($fp_no);
            $page = $this->form_pages[$fp_no];
            $i = $k+1;//actual page no.
            if ($n == 1)
            {
               $form->addElement(new Element_HTML("<div class=\"rmformpage_form_".$this->form_id."_".$this->form_number."\" id=\"rm_form_page_form_".$this->form_id ."_".$this->form_number. "_".$n."\">"));
               $form->addElement(new Element_HTML("<fieldset class='rmfieldset'>"));
               
               if(count($this->form_pages) > 1)
                 $form->addElement(new Element_HTML("<legend style='".$this->form_options->style_section."'>".$page."</legend>"));
               $this->hook_pre_field_addition_to_page($form, $n);
           
                    foreach ($this->fields as $field)
                    {
                       
                        $pf = $field->get_pfbc_field();
                            
                        if ($pf === null || $field->get_page_no() != $i)
                            continue;

                        if (is_array($pf))
                        {
                            foreach ($pf as $f)
                            {
                                if (!$f)
                                    continue;
                                $form->addElement($f);
                            }
                        } else
                            $form->addElement($pf);
                        
                    }
                    
                    $this->hook_post_field_addition_to_page($form, $n,$editing_sub);
                    $form->addElement(new Element_HTML("</fieldset>"));
                    $form->addElement(new Element_HTML("</div>"));
                    
            } else
            {
                $form->addElement(new Element_HTML("<div class=\"rmformpage_form_".$this->form_id."_".$this->form_number."\"id=\"rm_form_page_form_".$this->form_id ."_".$this->form_number. "_".$n."\" style=\"display:none\">"));
               $form->addElement(new Element_HTML("<fieldset class='rmfieldset'>"));
                 $form->addElement(new Element_HTML("<legend style='".$this->form_options->style_section."'>".$page."</legend>"));
               
                ?>
                
                    <?php
                    $this->hook_pre_field_addition_to_page($form, $n);
                    foreach ($this->fields as $field)
                    {
                        $pf = $field->get_pfbc_field();

                        if ($pf === null || $field->get_page_no() != $i)
                            continue;

                        if (is_array($pf))
                        {
                            foreach ($pf as $f)
                            {
                                if (!$f)
                                    continue;
                                $form->addElement($f);
                            }
                        } else
                            $form->addElement($pf);
                    }
                    $this->hook_post_field_addition_to_page($form, $n);
                     $form->addElement(new Element_HTML("</fieldset>"));
                    $form->addElement(new Element_HTML("</div>"));          
            }

            $n++;
        }

        
    }
    
    protected function prepare_button_for_render($form)
    {
        if ($this->service->get_setting('theme') != 'matchmytheme')
        {
            if(isset($this->form_options->style_btnfield))
                unset($this->form_options->style_btnfield);
        }
        $btn_label = $this->form_options->form_submit_btn_label;
        $max_pages = count($this->get_form_pages());
        if($max_pages > 1 && !$this->form_options->no_prev_button)
        $form->addElement(new Element_Button(RM_UI_Strings::get('LABEL_PREV_FORM_PAGE'), "button", array("style" => isset($this->form_options->style_btnfield)?$this->form_options->style_btnfield:null, "id"=>"rm_prev_form_page_button_".$this->form_id.'_'.$this->form_number, "onclick"=>'gotoprev_form_'.$this->form_id.'_'.$this->form_number.'()', "disabled"=>"1")));
        $form->addElement(new Element_Button($btn_label != "" ? $btn_label : "Submit", "button", array("style" => isset($this->form_options->style_btnfield)?$this->form_options->style_btnfield:null, "id"=>"rm_next_form_page_button_".$this->form_id.'_'.$this->form_number,"onclick"=>'gotonext_form_'.$this->form_id.'_'.$this->form_number.'()')));
        //$form->addElement(new Element_Button($btn_label != "" ? $btn_label : "Submit", "submit", array("bgColor" => "#$submit_btn_bgcolor", "fgColor" => "#$submit_btn_fgcolor")));
        
        $this->insert_JS($form);
    }
    
    protected function get_jqvalidator_config_JS()
    {
$str = <<<JSHD
        jQuery.validator.setDefaults({errorClass: 'rm-form-field-invalid-msg',
                                        ignore:[],
                                       errorPlacement: function(error, element) {
                                                            error.appendTo(element.closest('.rminput'));
                                                          }
                                    });
JSHD;
        return $str;
    }

    protected function insert_JS($form)
    {
        $max_page_count = count($this->get_form_pages());
        $form_identifier = "form_".$this->get_form_id();
        $form_id = $this->get_form_id();
        $validator_js = $this->get_jqvalidator_config_JS();
        
        $jqvalidate = RM_Utilities::enqueue_external_scripts('rm_jquery_validate', "https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js");
        $jqvalidate .= RM_Utilities::enqueue_external_scripts('rm_jquery_validate_add', "https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.min.js");
        
        $mainstr = <<<JSHD
                
   <pre class='rm-pre-wrapper-for-script-tags'><script>
        
   /*form specific onload functionality*/
jQuery(document).ready(function () {
if(jQuery("#{$form_identifier}_{$this->form_number} [name='rm_payment_method']").length>0 && jQuery("#{$form_identifier}_{$this->form_number} [name='rm_payment_method']:checked").val()=='stripe')                
    jQuery('#rm_stripe_fields_container_{$form_id}_{$this->form_number}').show();
   });
                
if (typeof window['rm_multipage'] == 'undefined') {

    rm_multipage = {
        global_page_no_{$form_identifier}_{$this->form_number}: 1
    };

}
else
 rm_multipage.global_page_no_{$form_identifier}_{$this->form_number} = 1;

function gotonext_{$form_identifier}_{$this->form_number}(){
	
        maxpage = {$max_page_count} ;
        {$validator_js}        
        
        var payment_method = jQuery('[name=rm_payment_method]:checked').val();
        
        if(typeof payment_method == 'undefined' || payment_method != 'stripe')
        {            
            elements_to_validate = jQuery("#rm_form_page_{$form_identifier}_{$this->form_number}_"+rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}+" :input").not('#rm_stripe_fields_container_{$form_id}_{$this->form_number} :input');
        }
        else
            var elements_to_validate = jQuery("#rm_form_page_{$form_identifier}_{$this->form_number}_"+rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}+" :input");
            
        if(elements_to_validate.length > 0)
        {
            var valid = elements_to_validate.valid();
                        
            if(!valid)
            {
                jQuery(document).find("input.rm-form-field-invalid-msg")[0].focus(); 
                return;
            }
        }
        
        /* Server validation for Username and Email field */
        for(var i=0;i<rm_validation_attr.length;i++){
            var validation_flag= true;
            jQuery("[" + rm_validation_attr[i] + "=0]").each(function(){
               validation_flag= false;
               return false;
            });
            
            if(!validation_flag)
              return;
        }
        
        
        rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}++;
        
        /*skip blank form pages*/
        while(jQuery("#rm_form_page_{$form_identifier}_{$this->form_number}_"+rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}+" :input").length == 0)
        {
        
            if(maxpage <= rm_multipage.global_page_no_{$form_identifier}_{$this->form_number})
            {
                    if(jQuery("#rm_form_page_{$form_identifier}_{$this->form_number}_"+rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}+" :input").length == 0){
                        jQuery("#rm_next_form_page_button_{$form_id}_{$this->form_number}").prop('type','submit');
                        jQuery("#rm_prev_form_page_button_{$form_id}_{$this->form_number}").prop('disabled',true);
                        return;
                    }        
                    else
                        break;
            }
        
            rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}++;
        }
            
	
	if(maxpage < rm_multipage.global_page_no_{$form_identifier}_{$this->form_number})
	{alert('about to change');
		rm_multipage.global_page_no_{$form_identifier}_{$this->form_number} = maxpage;
		jQuery("#rm_next_form_page_button_{$form_id}_{$this->form_number}").prop('type','submit');
                jQuery("#rm_prev_form_page_button_{$form_id}_{$this->form_number}").prop('disabled',true);
		return;
	}
	jQuery(".rmformpage_{$form_identifier}_{$this->form_number}").each(function (){
		var visibledivid = "rm_form_page_{$form_identifier}_{$this->form_number}_"+rm_multipage.global_page_no_{$form_identifier}_{$this->form_number};
		if(jQuery(this).attr('id') == visibledivid)
			jQuery(this).show();
		else
			jQuery(this).hide();                
	})  
                jQuery('.rmformpage_{$form_identifier}_{$this->form_number}').find(':input').filter(':visible').eq(0).focus();
    jQuery("#rm_prev_form_page_button_{$form_id}_{$this->form_number}").prop('disabled',false);
        rmInitGoogleApi();
}
    </script></pre>
JSHD;

$prev_button_str = <<<JSPBHD
<pre class='rm-pre-wrapper-for-script-tags'><script>
function gotoprev_{$form_identifier}_{$this->form_number}(){
	
	maxpage = {$max_page_count} ;
	rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}--;
        jQuery("#rm_next_form_page_button_{$form_id}_{$this->form_number}").attr('type','button');        
        
        /*skip blank form pages*/
        while(jQuery("#rm_form_page_{$form_identifier}_{$this->form_number}_"+rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}+" :input").length == 0)
        {
            if(1 >= rm_multipage.global_page_no_{$form_identifier}_{$this->form_number})
            {
                    if(jQuery("#rm_form_page_{$form_identifier}_{$this->form_number}_"+rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}+" :input").length == 0){
                        rm_multipage.global_page_no_{$form_identifier}_{$this->form_number} = 1;
                        jQuery("#rm_prev_form_page_button_{$form_id}_{$this->form_number}").prop('disabled',true);
                        return;
                    }        
                    else
                        break;
            }
        
            rm_multipage.global_page_no_{$form_identifier}_{$this->form_number}--;
        }
        
	jQuery(".rmformpage_{$form_identifier}_{$this->form_number}").each(function (){
		var visibledivid = "rm_form_page_{$form_identifier}_{$this->form_number}_"+rm_multipage.global_page_no_{$form_identifier}_{$this->form_number};
		if(jQuery(this).attr('id') == visibledivid)
			jQuery(this).show();
		else
			jQuery(this).hide();
	})
        jQuery('.rmformpage_{$form_identifier}_{$this->form_number}').find(':input').filter(':visible').eq(0).focus();
        if(rm_multipage.global_page_no_{$form_identifier}_{$this->form_number} <= 1)
        {
            rm_multipage.global_page_no_{$form_identifier}_{$this->form_number} = 1;
            jQuery("#rm_prev_form_page_button_{$form_id}_{$this->form_number}").prop('disabled',true);
        }
}
         
</script></pre>
JSPBHD;
            
        if($this->form_options->no_prev_button)
            $str = $jqvalidate.$mainstr;
        else
            $str = $jqvalidate.$mainstr.$prev_button_str;
        
        $form->addElement(new Element_HTML($str));
    }

}
