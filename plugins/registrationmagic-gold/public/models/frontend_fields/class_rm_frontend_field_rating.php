<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class_rm_frontend_field_ggeo
 *
 * @author RegistrationMagic
 */
class RM_Frontend_Field_Rating extends RM_Frontend_Field_Base
{

    private $api_key;

    public function __construct($id, $type,$field_name, $label, $options, $page_no,$is_primary = false, $extra_opts = null)
    {
        parent::__construct($id, $type,$field_name, $label, $options, $page_no, $is_primary, $extra_opts);
    }

    public function get_pfbc_field()
    {
        if ($this->pfbc_field)
            return $this->pfbc_field;
        else
        {
            $field_cls = 'Element_Rating';
            $label = $this->get_formatted_label();
                $this->pfbc_field = new $field_cls($label, $this->field_name, $this->field_options);
                
            return $this->pfbc_field;
        }
    }
}
