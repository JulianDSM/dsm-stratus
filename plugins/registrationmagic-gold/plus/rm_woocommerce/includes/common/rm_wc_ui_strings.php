<?php

/**
 * This class works as a repository of all the string resources used in product UI
 * for easy translation and management. 
 *
 * @author CMSHelplive
 */

class RM_WC_UI_Strings
{
    public static function get($identifier)
    {
        switch($identifier)
        {
            case 'LABEL_SHIPPING_ADDRESS':
                return __('Shipping Address','rm_woocommerce');
                
            case 'LABEL_BILLING_ADDRESS':
                return __('Billing Address','rm_woocommerce');
            
            case 'LABEL_ORDER':
                return __('Order','rm_woocommerce');    
            
            case 'LABEL_ORDERS':
                return __('Orders','rm_woocommerce');    
            
            case 'LABEL_ORDER_DETAILS':
                return __('Order details','rm_woocommerce');      
            
            case 'LABEL_DOWNLOADS':
                return __('Downloads','rm_woocommerce');
            
            case 'LABEL_DOWNLOAD':
                return __('Download','rm_woocommerce');    
            
            case 'LABEL_ADDRESSES':
                return __('Addresses','rm_woocommerce');
                
            case 'LABEL_VIEW':
                return __('View','rm_woocommerce');
            
            case 'LABEL_NAME':
                return __('Name','rm_woocommerce');
                
            case 'LABEL_TOTAL':
                return __('Total','rm_woocommerce');
                
            case 'LABEL_SUBTOTAL':
                return __('Subtotal','rm_woocommerce');
                
            case 'LABEL_SHIPPING':
                return __('Shipping','rm_woocommerce');
                
            case 'LABEL_DISCOUNT':
                return __('Discount','rm_woocommerce');
                
            case 'LABEL_TOTAL_DISCOUNT':
                return __('Total Discount','rm_woocommerce');
                
            case 'LABEL_ORDER_TOTAL':
                return __('Order Total','rm_woocommerce');
                
            case 'LABEL_COUPONS_USED':
                return __('Coupon(s) Used','rm_woocommerce');
                
            case 'LABEL_PRODUCT_NAME':
                return __('Product Name','rm_woocommerce');
                
            case 'LABEL_QUANTITY':
                return __('Quantity','rm_woocommerce');
                
            case 'LABEL_COST':
                return __('Cost','rm_woocommerce');
                
            case 'NOTICE_NO_SHIPPING_ADDRESS_USER':
                return __('User has not set up shipping address yet.','rm_woocommerce');
                
            case 'NOTICE_NO_BILLING_ADDRESS_USER':
                return __('User has not set up billing address yet.','rm_woocommerce');
                
            case 'LABEL_REMAINING_DOWNLOADS':
                return __('Remaining Downloads','rm_woocommerce');
                
            case 'LABEL_ACCESS_EXPIRES':
                return __('Access Expires','rm_woocommerce');
                
            case 'LABEL_ORDER_STATUS':
                return __('Status','rm_woocommerce');
                
            case 'LABEL_AMOUNT':
                return __('Amount','rm_woocommerce');
                
            case 'LABEL_PLACED_ON':
                return __('Placed on','rm_woocommerce');
                
            case 'LABEL_ITEMS':
                return __('Items','rm_woocommerce');
                
            case 'LABEL_REMAINING_DLS_UNLIMITED':
                return __('Unlimited','rm_woocommerce');
                
            case 'LABEL_ACCESS_EXPIRES_NEVER':
                return __('Never','rm_woocommerce');
                
            case 'LABEL_WOO_REG_FORM' : 
                return __ ('Registration Form', 'rm_woocommerce');
                
            case 'HELP_WOO_REG_FORM' : 
                return __ ('Once selected, fields of this form will appear in the default WooCommerce registration page.', 'rm_woocommerce');
                
            case 'LABEL_RM_GLOBAL_SETTING_MENU' : 
                return __ ('WooCommerce Integration', 'rm_woocommerce');
                
            case 'SUBTITLE_RM_GLOBAL_SETTING_MENU' : 
                return __ ('Integrate forms inside WooCommerce', 'rm_woocommerce');
                
            case 'LABEL_GO_SHOP' : 
                return __ ('Go shopping!', 'rm_woocommerce');
                
            case 'LABEL_CART_EMPTY' : 
                return __ ('No item in the cart', 'rm_woocommerce');
                
            case 'LABEL_TOTAL_REVENUE':
                return __('Total Revenue','rm_woocommerce');
                
            case 'LABEL_ENABLE_CART_IN_FAB':
                return __('Show cart on popup menu','rm_woocommerce');
                
            case 'HELP_ENABLE_CART_IN_FAB':
                return __('Enables quick access to the cart from the front-end Magic Popup menu.','rm_woocommerce');
                
            case 'LABEL_ORDER_NOTES':
                return __('Order Notes','rm_woocommerce');
                
            case 'LABEL_ORDER_NOTE_FOOTER':
                return __('Added by %s on %s','rm_woocommerce');
                
            case 'ALERT_GUEST_CHECKOUT_ENABLED':
                return __('Guest Checkout is enabled in WooCommerce. Disable it to display RegistrationMagic form for registration during checkout. <a href="%s" target="_blank">Click here</a> to configure.','rm_woocommerce');
            
             case 'NAME_WC':
                return __('WooCommerce','rm_woocommerce');
                
             case 'WC_ERROR':
                return __("<div class='rmnotice'>Oops!! Something went wrong.<ul><li>Possible causes:-</li><li><a target='_blank' href='https://wordpress.org/plugins/woocommerce/'>Woocommerce</a> is not installed/active.</li></ul></div>", 'registrationmagic-gold');
                 
            case 'WC_FORM_SETTING_TEXT':
             return __("<div class='rmnotice'>You can configure Woocommerce from <a href='?page=rm_wc_settings'>Global Settings->Woocommerce Integration</a></div>",'rm_woocommerce');
                    
            default:
                return __("NO STRING FOUND (rmwc)", 'rm_woocommerce');
        }
    }
}