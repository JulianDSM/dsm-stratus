<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( !class_exists( 'RM_WC_Fields' ) ) :

abstract class RM_WC_Fields{
    
    protected $form=null;
    
    abstract public function render();
    
    /**
     * 
     * @param  Array $exclude (Excludes fields on type basis)
     * @param Boolean $render (True if it is called for render purpose)
     * @return Array 
     */
    protected function get_fields($exclude=array(),$render= false){
        //Initiate form service
        $form_service= new RM_Front_Form_Service();
        $fields = array();
        $form_id= $this->form->get_form_id();
        $form= $this->form;
        $db_fields = $form_service->get_all_form_fields($form_id);

        if($db_fields)
        { 
           foreach($db_fields as $db_field)
           {   
               $field_options = maybe_unserialize($db_field->field_options);
               $form_options = $form->get_form_options();
               $field_type= str_replace('-','',strtolower($db_field->field_type));
              
               
               if($render && $db_field->is_field_primary==1):
                   continue;
               endif;
               
               if(count($exclude)>0 && in_array($field_type, $exclude)):
                   continue;
               endif;
               

               $opts = $form_service->set_properties($field_options);
               $field_name= $db_field->field_type."_".$db_field->field_id; 
               $opts['id']= $field_name;
               $opts['value']= '';
               
               $field_default_values = maybe_unserialize($field_options->field_default_value);
               if(!empty($field_default_values))
               {
		 if($_SERVER['REQUEST_METHOD'] != 'POST')    
                   $opts['value']= $field_default_values;
               }
               
               if($db_field->is_field_primary!=1 && isset($_POST[$field_name]) && strtolower($field_type)!=="file"):
                       $opts['value'] = $_POST[$field_name];
               endif;

               
               if($db_field->is_field_primary==1):
                   $opts['value'] = isset($_POST['email'])?$_POST['email']:(isset($_POST['billing_email'])?$_POST['billing_email']:null);
               endif;
               
               if(isset($_FILES[$field_name])):
                   $opts['value'] = $_FILES[$field_name];
               endif;
               
               $field_factory= new RM_Field_Factory($db_field,$opts);
               $field_type= str_replace('-','',strtolower($db_field->field_type));

               if(is_callable(array($field_factory,"create_".$field_type."_field"))):
                    $fields[$field_name] = call_user_func_array(array($field_factory,"create_".$field_type."_field"),array());
               else:
                   $fields[$field_name] = call_user_func_array(array($field_factory,"create_default_field"),array());
               endif;

           }   
         } 
         return $fields;
         
    }
    
    abstract public function save_submission($username, $email, $validation_errors);
    
    protected function validate($fields,$validation_errors){ 
        if(count($fields)>0):
            foreach($fields as $field):
                 $field_options= $field->get_field_options();
                 // Handle array values
                  if(is_array($field_options['value']))
                 {   
                      if(empty($field_options['value'][0]) && empty($field_options['value']['original']))
                          $field_options['value']= "";
                 }
                
                 $field->is_valid($field_options['value'],$this->form->get_form_id(),$validation_errors);
            endforeach;
        endif;
      
    }
    
    protected function get_db_data($type='dbonly'){
        $fields= $this->get_fields();
        $form= new RM_Frontend_Form_Contact($this->form);
        $db_data = $form->get_prepared_data($_POST, $type,$fields);
        return $db_data;
    }
    
    protected function render_js(array $fields){
        $urls = array();
        $deps = array();
        foreach($fields as $field):

            $element= $field->get_pfbc_field(); 
            if (is_array($element)) {
                    foreach ($element as $e) {
                        $elementUrls = $e->getJSFiles();
                        $elementDeps = $e->getJSDeps();
                        if (is_array($elementDeps))
                            $deps = array_merge($deps, $elementDeps);

                        if (is_array($elementUrls))
                            $urls = array_merge($urls, $elementUrls);
                    }
                }
                else {
                    $elementUrls = $element->getJSFiles();
                    $elementDeps = $element->getJSDeps();
                    if (is_array($elementDeps))
                        $deps = array_merge($deps, $elementDeps);

                    if (is_array($elementUrls))
                        $urls = array_merge($urls, $elementUrls);
                }

            endforeach;
        
        /* This section prevents duplicate js files from being loaded. */
        if (!empty($urls))
        {

            $urls = array_unique($urls);
            foreach ($deps as $dep)
            {
                if(isset($urls[$dep]))
                {
                echo RM_Utilities::enqueue_external_scripts($dep, $urls[$dep]);
                unset($urls[$dep]);
                }
            }

            foreach ($urls as $handle => $url)
                echo RM_Utilities::enqueue_external_scripts($handle, $url);
        }

     // $element->renderJS();
        
    }
    
    protected function render_css(array $fields){
        $urls = array();
        foreach($fields as $field):
            $element= $field->get_pfbc_field();
            if(is_array($element)){
                foreach($element as $e){
                    $elementUrls = $e->getCSSFiles();
                    if (is_array($elementUrls))
                    $urls = array_merge($urls, $elementUrls);
                }
            }
            else{
                $elementUrls = $element->getCSSFiles();
                    if (is_array($elementUrls))
                    $urls = array_merge($urls, $elementUrls);
            }
            
            
        endforeach;
        
        /* This section prevents duplicate css files from being loaded. */
        if (!empty($urls))
        {
            $urls = array_values(array_unique($urls));
            foreach ($urls as $url)
                echo '<link type="text/css" rel="stylesheet" href="', $url, '"/>';
        }
        //$element->renderCSS();
    }
    
}

endif;
